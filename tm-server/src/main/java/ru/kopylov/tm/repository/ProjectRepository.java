package ru.kopylov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager manager) {
        super(manager);
    }

    @Override
    public void remove(@NotNull final String id) {
        manager.remove(manager.find(Project.class, id));
    }

    @NotNull
    @Override
    public Project findOne(@NotNull final String id) {
        return manager.find(Project.class, id);
    }

    @NotNull
    public List<Project> findAll() {
        return manager.createQuery("select p from Project p", Project.class).getResultList();
    }

    @NotNull
    public List<Project> findAllByUserId(@NotNull final String userId) {
        return manager.createQuery("select p from Project p where p.user.id = :userId", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Project> findAllByUserIdOrderByDateStart(@NotNull final String userId) {
        return manager
                .createQuery("select p from Project p where p.user.id = :userId order by p.dateStart asc", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Project> findAllByUserIdOrderByDateFinish(@NotNull final String userId) {
        return manager
                .createQuery("select p from Project p where p.user.id = :userId order by p.dateFinish asc", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Project> findAllByUserIdOrderByState(@NotNull final String userId) {
        return manager
                .createQuery("select p from Project p where p.user.id = :userId order by p.state asc", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAllByUserId(userId);
        for (@NotNull final Project project : projects) {
            manager.remove(project);
        }
    }

    @NotNull
    public List<Project> findByContent(@NotNull final String content) {
        return manager.createQuery("select p from Project p where p.name like concat('%', :content, '%') " +
                "or p.description like concat('%', :content, '%')", Project.class)
                .setParameter("content", content).getResultList();
    }

}
