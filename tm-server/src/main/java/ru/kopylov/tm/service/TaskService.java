package ru.kopylov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.enumerated.TypeSort;
import ru.kopylov.tm.repository.TaskRepository;
import ru.kopylov.tm.util.DateUtil;
import ru.kopylov.tm.util.HibernateUtil;
import ru.kopylov.tm.util.TaskComparator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class TaskService extends AbstractService implements ITaskService {

    @Nullable
    private ITaskRepository taskRepository;

    @Nullable
    private EntityManagerFactory factory;

    @Nullable
    private EntityManager manager;

    public TaskService(@NotNull ServiceLocator bootstrap) {
        super(bootstrap);
    }

    public boolean persist(@Nullable final Task task) {
        if (task == null ) return false;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            taskRepository.persist(task);
            manager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
    }

    public boolean persist(@Nullable final String currentUserId, @Nullable final String taskName) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final Task task = new Task();
        task.setName(taskName);
        @Nullable final User user = bootstrap.getUserService().findOne(currentUserId);
        task.setUser(user);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        return persist(task);
    }

    @Nullable
    public Task findOne(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            return taskRepository.findOne(taskId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            manager.close();
            factory.close();
        }
    }

    @NotNull
    public List<Task> findAll() {
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            @Nullable final List<Task> tasks = taskRepository.findAll();
            manager.getTransaction().commit();
            return tasks;
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            manager.close();
            factory.close();
        }
    }

    @NotNull
    public List<Task> findAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return Collections.emptyList();
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            @Nullable final List<Task> tasks = taskRepository.findAllByUserId(currentUserId);
            manager.getTransaction().commit();
            return tasks;
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            manager.close();
            factory.close();
        }
    }

    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            return taskRepository.findAllByProjectId(projectId);
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            manager.close();
            factory.close();
        }
    }

    @Nullable
    public List<Task> findAll(@Nullable final String currentUserId, @Nullable final String typeSort) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            if (TypeSort.CREATE_DATE.getDisplayName().equals(typeSort) || typeSort == null || typeSort.isEmpty())
                return taskRepository.findAllByUserId(currentUserId);
            if (TypeSort.START_DATE.getDisplayName().equals(typeSort)) {
                return taskRepository.findAllByUserIdOrderByDateStart(currentUserId);
            }
            if (TypeSort.FINISH_DATE.getDisplayName().equals(typeSort)) {
                return taskRepository.findAllByUserIdOrderByDateFinish(currentUserId);
            }
            if (TypeSort.STATE.getDisplayName().equals(typeSort)) {
                return taskRepository.findAllByUserIdOrderByState(currentUserId);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            manager.close();
            factory.close();
        }
    }

    public boolean merge(@Nullable final Task task) {
        if (task == null ) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            taskRepository.merge(task);
            manager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
    }

    public boolean merge(
            @Nullable final String currentUserId,
            @NotNull final Integer taskNumber,
            @Nullable final String taskName,
            @Nullable final String taskDescription,
            @Nullable final String taskDateStart,
            @Nullable final String taskDateFinish,
            @Nullable final Integer stateNumber
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            taskList = taskRepository.findAllByUserId(currentUserId);
            manager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        if (taskName != null && !taskName.isEmpty()) task.setName(taskName);
        if (taskDescription != null && !taskDescription.isEmpty())
            task.setDescription(taskDescription);
        if (taskDateStart != null && !taskDateStart.isEmpty()) {
            @NotNull final Date dateStart = DateUtil.stringToDate(taskDateStart);
            task.setDateStart(dateStart);
        }
        if (taskDateFinish != null && !taskDateFinish.isEmpty()) {
            @NotNull final Date dateFinish = DateUtil.stringToDate(taskDateFinish);
            task.setDateFinish(dateFinish);
        }
        if (stateNumber != null) {
            if (stateNumber < 1 || stateNumber > State.values().length) return false;
            task.setState(State.values()[stateNumber-1]);
        }
        return merge(task);
    }

    public boolean remove(@Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) return false;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            taskRepository.remove(taskId);
            manager.getTransaction().commit();
            return true;
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return false;
        } finally {
            manager.close();
            factory.close();
        }
    }

    public boolean remove(@Nullable final String currentUserId, @NotNull final Integer taskNumber) {
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        @NotNull final List<Task> taskList = findAll(currentUserId);
        if (taskNumber < 1 || taskNumber > taskList.size()) return false;
        @NotNull final Task task = taskList.get(taskNumber - 1);
        return remove(task.getId());
    }

    public void removeAll(@Nullable final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            taskRepository.removeAllByUserId(currentUserId);
            manager.getTransaction().commit();
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            manager.close();
            factory.close();
        }
    }

    @NotNull
    public List<Task> findByContent(@Nullable String content) {
        if (content == null || content.isEmpty()) return Collections.emptyList();
        factory = HibernateUtil.factory();
        manager = factory.createEntityManager();
        manager.getTransaction().begin();
        try {
            taskRepository = new TaskRepository(manager);
            return taskRepository.findByContent(content);
        } catch (Exception e) {
            manager.getTransaction().rollback();
            e.printStackTrace();
            return Collections.emptyList();
        } finally {
            manager.close();
            factory.close();
        }
    }

}
