package ru.kopylov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDto extends AbstractDto {

    @Nullable
    private Long timestamp = System.currentTimeMillis();

    @Nullable
    private String userId;

    @Nullable
    private String signature;

}
