package ru.kopylov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.api.endpoint.IDataEndpoint;
import ru.kopylov.tm.api.endpoint.IProjectEndpoint;
import ru.kopylov.tm.api.endpoint.ITaskEndpoint;
import ru.kopylov.tm.api.endpoint.IUserEndpoint;
import ru.kopylov.tm.api.service.*;
import ru.kopylov.tm.endpoint.DataEndpoint;
import ru.kopylov.tm.endpoint.ProjectEndpoint;
import ru.kopylov.tm.endpoint.TaskEndpoint;
import ru.kopylov.tm.endpoint.UserEndpoint;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.UserRepository;
import ru.kopylov.tm.service.*;
import ru.kopylov.tm.util.DataBaseUtil;
import ru.kopylov.tm.util.HashUtil;
import ru.kopylov.tm.util.HibernateUtil;
import ru.kopylov.tm.util.MyBatisUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.sql.Connection;

@Getter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @NotNull
    private final IUserService userService = new UserService(this);

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(this, propertyService);

    @NotNull
    public Connection getConnection() throws Exception {
        return DataBaseUtil.getConnect(propertyService.getDbHost(), propertyService.getDbLogin(), propertyService.getDbPassword());
    }

    public void init() {
        try {
            propertyService.init();
            userInit();
        }catch (Exception e) {
            e.printStackTrace();
        }
        endpointRegistry();
    }

    private void endpointRegistry() {
        @NotNull final IUserEndpoint userEndpoint = new UserEndpoint(sessionService, this);
        Endpoint.publish(userEndpoint.getUrl(), userEndpoint);
        System.out.println(userEndpoint.getUrl());
        @NotNull final IProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, this);
        Endpoint.publish(projectEndpoint.getUrl(), projectEndpoint);
        System.out.println(projectEndpoint.getUrl());
        @NotNull final ITaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, this);
        Endpoint.publish(taskEndpoint.getUrl(), taskEndpoint);
        System.out.println(taskEndpoint.getUrl());
        @NotNull final IDataEndpoint dataEndpoint = new DataEndpoint(sessionService, this);
        Endpoint.publish(dataEndpoint.getUrl(), dataEndpoint);
        System.out.println(dataEndpoint.getUrl());

    }

    @NotNull
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = propertyService.getDbLogin();
        @Nullable final String password = propertyService.getDbPassword();
        @Nullable final String url = propertyService.getDbHost();
        @Nullable final String driver = propertyService.getDbDriver();
        return MyBatisUtil.getSqlSessionFactory(user, password, url, driver);
    }

    private void userInit() throws Exception {
        final EntityManagerFactory factory = HibernateUtil.factory();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        final User test = new User();
        test.setId("7966e8d4-1fd9-4665-aa1a-ae3a5c5f5b31");
        test.setLogin("test");
        test.setRole(TypeRole.ADMIN);
        test.setPassword(HashUtil.hash("test"));
        em.merge(test);

        @NotNull final User user = new User();
        user.setId("7ff3bb1a-1f4c-4468-a1f0-2111f578f8cf");
        user.setLogin("user");
        user.setRole(TypeRole.USER);
        user.setPassword(HashUtil.hash("222222"));
        UserRepository userRepository = new UserRepository(em);
        userRepository.merge(user);
        em.getTransaction().commit();

        System.out.println(userRepository.findAll());

        em.close();
        factory.close();
    }

}
