package ru.kopylov.tm.api;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;

public interface ITerminalService {

    @Nullable
    String getReadLine() throws IOException;

}
