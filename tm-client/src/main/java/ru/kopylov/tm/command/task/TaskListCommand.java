package ru.kopylov.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.endpoint.TaskDto;
import ru.kopylov.tm.util.CommandUtil;

import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println("\nFor sort list type any of these commands:" +
                "\n-     create-date" +
                "\n-     start-date" +
                "\n-     finish-date" +
                "\n-     state" +
                "\npress [Enter] for default sort.");
        @Nullable final String typeSort = bootstrap.getTerminalService().getReadLine();
        @Nullable final List<TaskDto> tasks = bootstrap.getTaskEndpoint().getTaskList(bootstrap.getToken(), typeSort);
        if (tasks == null) {
            System.out.println("Command not exist.");
            return;
        }
        CommandUtil.printTaskListWithParam(tasks);
    }

}
